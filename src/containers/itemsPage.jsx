import { connect }                          from 'react-redux';
import { fetchItemsIfNeeded }               from '../actions/items';
import Items                                from '../components/items';
import * as options                         from '../actions/options';
import { editItem, removeItem, insertItem } from '../actions/item';

const mapStateToProps = state => ({
  items: state.items.data,
  token: state.login.token,
  optionsId: state.options.id
});

const mapDispatchToProps = dispatch => ({
  onItemsFetch: dispatch(fetchItemsIfNeeded()),
  onOpenOptions: id => dispatch(options.openOptions(id)),
  onCloseOptions: () => dispatch(options.closeOptions()),
  onEditItem: dispatch(editItem()),
  onInsertItem: () => dispatch(insertItem()),
  onRemoveItem: id => dispatch(removeItem(id)),
  onAddOption: id => dispatch(options.addOption(id)),
  onRemoveOption: (id, index) => dispatch(options.removeOption(id, index)),
  onEditOption: dispatch(options.editOption())
});

export default connect(mapStateToProps, mapDispatchToProps)(Items);
