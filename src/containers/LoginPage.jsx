import { connect } from 'react-redux';
import login       from '../actions/login';
import Login       from '../components/login';

const mapDispatchToProps = dispatch => ({
  onLogin: login(dispatch)
});

const mapStateToProps = state => ({
  isLoggedIn: state.login.isLoggedIn
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
