export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_DENIED = 'LOGIN_DENIED';
export const LOGIN_SUCCESSFULLY = 'LOGIN_SUCCESSFULLY';

export const ITEMS_REQUEST_STARTED = 'ITEMS_REQUEST_STARTED';
export const ITEMS_REQUEST_FAILED = 'ITEMS_REQUEST_FAILED';
export const ITEMS_REQUEST_SUCCESSFULLY = 'ITEMS_REQUEST_SUCCESSFULLY';

export const ITEMS_UPDATE = 'ITEMS_UPDATE';

export const OPEN_OPTIONS = 'OPEN_OPTIONS'
export const CLOSE_OPTIONS = 'CLOSE_OPTIONS'

export const ADD_OPTION = 'ADD_OPTION';  
export const EDIT_OPTIONS = 'EDIT_OPTION';
export const DELETE_OPTIONS = 'DELETE_OPTION'

export const ITEM_INSERT = 'ITEM_INSERT'
export const ITEM_EDIT = 'ITEM_EDIT';
export const ITEM_REMOVE = 'ITEM_REMOVE';
