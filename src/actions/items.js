import * as types from '../constants';

const itemsRequestStarted = () => ({ type: types.ITEMS_REQUEST_STARTED });

const itemsRequestFailed = error => ({ type: types.ITEMS_REQUEST_FAILED, error });

const itemsRequestSuccessfully = data => ({ type: types.ITEMS_REQUEST_SUCCESSFULLY, data });

export const itemsDidUpdate = () => ({ type: types.ITEMS_UPDATE });

export const fetchItems = () => async dispatch => {
  try {
    const res = await fetch('http://127.0.0.1:3000/items');
    dispatch(itemsRequestSuccessfully(await res.json()))
  } catch (e) {
    dispatch(itemsRequestFailed(e.data.message))
  }
}

export const fetchItemsIfNeeded = () => (dispatch, getState) => {
  const { items } = getState();

  if (items.data.length === 0 || items.didUpdated) {
    dispatch(itemsRequestStarted())
    dispatch(fetchItems())
  } else {
    return Promise.resolve()
  }
} 
