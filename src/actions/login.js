import * as types from '../constants';
import axios      from 'axios';

const loginRequest = (login, password) => ({
  type: types.LOGIN_REQUEST, login, password
});

const loginDenied = () => ({
  type: types.LOGIN_DENIED
});

const loginSuccessfully = (token, login) => ({
  type: types.LOGIN_SUCCESSFULLY, token, login
});

export default dispatch => (login, password) => {
  dispatch(loginRequest(login, password))

  axios.post("http://127.0.1:3000/login", {
    login,
    password
  }).then(token => dispatch(loginSuccessfully(token.data, login)))
    .catch(err => dispatch(loginDenied()))
}