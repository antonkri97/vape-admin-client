import * as types from '../constants';
import { itemsDidUpdate, fetchItemsIfNeeded } from './items';

const insert = () => ({ type: types.ITEM_INSERT });

const edit = id => ({ type: types.ITEM_EDIT, id });

const remove = id => ({ type: types.ITEM_REMOVE, id });

const fetchItem = async (dispatch, url, method, token) => {
  try {
    await fetch(url, {
      headers: { Authorization: token },
      method
    });

    dispatch(itemsDidUpdate());
    dispatch(fetchItemsIfNeeded());
  } catch (e) {
    console.log(e.data.message);
  }
}

export const editItem = () => (dispatch, getState) => (id, options) => async item => {
  dispatch(edit(id))

  const url = `http://127.0.0.1:3000/private/item/${id}`;
  const body = JSON.stringify(Object.assign({}, item, { options: options }));
  const config = { 
    headers: { Authorization: getState().login.token, 'Content-Type': 'application/json' }, 
    method: 'put',
    body
  };

  try {
    await fetch(url, config);

    dispatch(itemsDidUpdate());
    dispatch(fetchItemsIfNeeded());
  } catch (e) {
    console.log(e.data.message)
  }
}

export const removeItem = id => (dispatch, getState) => {
  dispatch(remove(id));

  const url = `http://127.0.0.1:3000/private/item/${id}`;

  fetchItem(dispatch, url, 'delete', getState().login.token)
};

export const insertItem = () => (dispatch, getState) => {
  dispatch(insert());
  const url = 'http://127.0.0.1:3000/private/item/empty';

  fetchItem(dispatch, url, 'post', getState().login.token)
};
