import * as types from '../constants';
import { fetchItemsIfNeeded, itemsDidUpdate } from './items';

export const openOptions = id => ({ type: types.OPEN_OPTIONS, id });

export const closeOptions = () => ({ type: types.CLOSE_OPTIONS });

const add = id => ({ type: types.ADD_OPTION, id });

const edit = (id, index, option) => ({
  type: types.EDIT_OPTIONS,
  index,
  option
});

const remove = (id, index) => ({ type: types.DELETE_OPTIONS, id, index });

const fetchOption = (dispatch, url, method, token, body) => {
  fetch(url, {
    headers: { Authorization: token },
    method,
    body: JSON.stringify(body)
  }).then(res => {
      dispatch(itemsDidUpdate())
      dispatch(fetchItemsIfNeeded())
    })
    .catch(err => console.log(err))
}

export const addOption = id => (dispatch, getState) => {
  dispatch(add(id))

  const url = `http://127.0.0.1:3000/private/item/option/${id}`;
  const { token } = getState().login;

  fetchOption(dispatch, url, 'put', token)
}

export const removeOption = (id, index) => (dispatch, getState) => {
  dispatch(remove(id, index))

  const url = `http://127.0.0.1:3000/private/item/option/${id}/${index}`;

  fetchOption(dispatch, url, 'delete', getState().login.token);
}

export const editOption = () => (dispatch, getState) => (id, index) => option=> {
  dispatch(edit(id, index, option))

  const url = `http://127.0.0.1:3000/private/item/option/${id}/${index}`;

  fetchOption(dispatch, url, 'put', getState().login.token, option)
}
