import * as types from '../constants';

const initialState = {
  id: '-1'
};

export default (state = initialState, action) => {
  switch(action.type) {
    case types.OPEN_OPTIONS:
      return { id: action.id};
    case types.CLOSE_OPTIONS:
      return { id: '-1' };
    default:
      return state;
  }
};
