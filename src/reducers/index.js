import { combineReducers } from 'redux';
import login               from './login';
import items               from './items';
import options             from './options';
import item                from './item';

export default combineReducers({
  login,
  items,
  options,
  item
});
