import * as types from '../constants';

const initialState = {
  data: [],
  loading: false,
  error: null,
  didUpdated: false
};

export default (state = initialState, action) => {
  switch(action.type) {
    case types.ITEMS_REQUEST_STARTED:
      return Object.assign({}, state, { loading: true });
    case types.ITEMS_REQUEST_FAILED:
      return Object.assign({}, state, { loading: false, error: action.error })
    case types.ITEMS_REQUEST_SUCCESSFULLY:
      return Object.assign({}, state, { data: action.data, loading: false, error: null, didUpdated: false });
    case types.ITEMS_UPDATE:
      return Object.assign({}, state, { didUpdated: true });
    default:
      return state
  }
};
