import * as types from '../constants';

const initialState = {
  isLoggedIn: false,
  loading: false,
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0OTU1MDk5NTJ9.VFYT8s2WZc0V1lCjdgUbyCdDzJxJKyNsVrxSmPZdb98',
  login: ''
};

export default (state = initialState, action) => {
  switch(action.type) {
    case types.LOGIN_REQUEST:
      return Object.assign({}, state, { loading: true });
    case types.LOGIN_SUCCESSFULLY:
      return { isLoggedIn: true, loading: false, token: action.token, login: action.login };
    case types.LOGIN_DENIED:
      return Object.assign({}, state, { loading: false })
    default:
      return state
  }
};
