import * as types from '../constants';

const initialState = {
  id: 0
}

export default (state = initialState, action) => {
  switch(action.type) {
    case types.ITEM_EDIT:
      return { id: action.id };
    case types.ITEM_REMOVE:
      return { id: action.id };
    case types.ITEM_INSERT:
      return state;
    default:
      return state;
  }
};
