import React                                                    from 'react';
import PropTypes                                                from 'prop-types'
import { FormGroup, FormControl, ControlLabel, HelpBlock, Col } from 'react-bootstrap';

const FieldGroup = ({ id, cols, label, help, ...props }) => (
  <Col sm={cols}>
    <FormGroup controlId={id}>
      { label && <ControlLabel>{label}</ControlLabel> }
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  </Col>
);

FieldGroup.propTypes = {
  id: PropTypes.string.isRequired,
  cols: PropTypes.number,
  label: PropTypes.string,
  help: PropTypes.string
};

export default FieldGroup;
