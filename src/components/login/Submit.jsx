import React      from 'react';
import PropTypes  from 'prop-types';
import { Button } from 'react-bootstrap';

const Submit = ({ onLogin }) => (
  <div className='text-center'>
    <Button onClick={onLogin}>
      Login
    </Button>
  </div>
)

Submit.propTypes = {
  onLogin: PropTypes.func.isRequired
};

export default Submit;
