import React      from 'react';
import FieldGroup from '../common/FieldGroup';

const Password = ({...props}) => (
  <FieldGroup
    id='formControlPassword'
    label='password'
    cols={6}
    type='password'
    {...props}
  />
);

export default Password;
