import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import Username             from './Username';
import Password             from './Password';
import Submut               from './Submit';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount() {
    this.setState({
      login: 'admin',
      password: 'BYMvqNH7CvnTTMsLaETxDYnCedDLo'
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });

    console.log(this.state.login)
  } 

  render() {
    const { onLogin } = this.props;
    const { login, password } = this.state

    return (
      <div>
        <Username name='login' defaultValue='admin' onChange={this.handleChange} />
        <Password name='password' defaultValue='BYMvqNH7CvnTTMsLaETxDYnCedDLo'  onChange={this.handleChange} />
        <Submut onLogin={() => onLogin(login, password)} />
      </div>
    )
  }
}

Login.propTypes = {
  onLogin: PropTypes.func.isRequired
};

export default Login;
