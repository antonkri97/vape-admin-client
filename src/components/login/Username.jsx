import React      from 'react';
import FieldGroup from '../common/FieldGroup';

const Username = ({...props}) => (
  <FieldGroup 
    id='formControlUsername'
    label='username' 
    cols={6}
    type='text'
    {...props}
  />
);

export default Username;
