import React       from 'react';
import PropTypes   from 'prop-types';
import SaveEdit    from './SaveEdit';
import DeleteItem  from './DeleteItem';
import EditOptions from './EditOptions';

const style = {
  'margin': '20px'
}

const Actions = ({ onOpenOptions, onEditItem, onRemoveItem }) => (
  <td>
    <SaveEdit onSaveEdit={onEditItem} style={style}/>
    <DeleteItem onRemoveOption={onRemoveItem} style={style}/>
    <EditOptions onOpenOptions={onOpenOptions} style={style}/>
  </td>
);

Actions.propTypes = {
  onOpenOptions: PropTypes.func.isRequired,
  onEditItem: PropTypes.func.isRequired,
  onRemoveItem: PropTypes.func.isRequired
};

export default Actions;
