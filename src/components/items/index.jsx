import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import { Table }            from 'react-bootstrap';
import Header               from './Header';
import Items                from './Items';
import Options              from './options';
import AddItem              from './AddItem';

class ItemsTable extends Component {
  render() {
    const { 
      items, 
      onOpenOptions, 
      optionsId, onCloseOptions, onAddOption, onRemoveOption, onEditOption, onEditItem, onRemoveItem, onInsertItem
    } = this.props;
    
    let openedOption = null;

    if (items !== null && optionsId !== '-1') {
      openedOption = items.find(i => i._id === optionsId);
    }

    return (
      <div>
        <Options
          id={optionsId}
          options={openedOption && openedOption.options}
          onClose={onCloseOptions}
          onAddOption={onAddOption}
          onRemoveOption={onRemoveOption}
          onEditOption={onEditOption}
        />
        <Table striped bordered condensed hover>
          <Header />
          <Items items={this.props.items} onOpenOptions={onOpenOptions} onEditItem={onEditItem} onRemoveItem={onRemoveItem}/>
        </Table>
        <AddItem onAddItem={onInsertItem} />
      </div>
    );
  }
}

ItemsTable.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      vendor: PropTypes.string,
      description: PropTypes.string,
      options: PropTypes.arrayOf(PropTypes.shape({
        capacity: PropTypes.number,
        nicotine: PropTypes.number,
        price: PropTypes.number,
        quantity: PropTypes.number
      })),
      largePhoto: PropTypes.string,
      mediumPhoto: PropTypes.string
    })
  ),
  onOpenOptions: PropTypes.func.isRequired,
  onCloseOptions: PropTypes.func.isRequired,
  optionsId: PropTypes.string.isRequired,
  onEditItem: PropTypes.func.isRequired,
  onRemoveItem: PropTypes.func.isRequired,
  onAddOption: PropTypes.func.isRequired,
  onRemoveOption: PropTypes.func.isRequired,
  onEditOption: PropTypes.func.isRequired,
  onInsertItem: PropTypes.func.isRequired
};

export default ItemsTable;
