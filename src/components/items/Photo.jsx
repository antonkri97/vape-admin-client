import React      from 'react';
import FieldGroup from '../common/FieldGroup';
import PropTypes  from 'prop-types';

const Photo = ({ size, value, ...props }) => (
  <td>
    <FieldGroup
      id={`${size}Photo`}
      type='text'
      defaultValue={value}
      {...props}
    />
  </td>
);

Photo.propTypes = {
  size: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};

export default Photo;
