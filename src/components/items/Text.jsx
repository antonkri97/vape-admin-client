import React from 'react';
import PropTypes from 'prop-types';
import FieldGroup from '../common/FieldGroup';

const Text = ({ formId, value, ...props }) => (
  <td>
    <FieldGroup
      id={formId}
      type='text'
      defaultValue={value}
      {...props}
    />
  </td>
);

Text.propTypes = {
  formId: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
};

export default Text;
