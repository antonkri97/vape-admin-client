import React from 'react';
import PropTypes from 'prop-types';
import { Button, Row } from 'react-bootstrap';
 
const SaveEdit = ({ onSaveEdit, style }) => (
  <Row>
    <Button onClick={onSaveEdit} style={style}>
      Сохранить
    </Button>
  </Row>
);

SaveEdit.propTypes = {
  onSaveEdit: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired
};

export default SaveEdit;
