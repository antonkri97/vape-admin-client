import React from 'react';
import PropTypes from 'prop-types';
import { Button, Row } from 'react-bootstrap';
 
const EditOptions = ({ onOpenOptions, style }) => (
  <Row>
    <Button onClick={onOpenOptions} style={style}>
      Открыть опции
    </Button>
  </Row>
);

EditOptions.propTypes = {
  onOpenOptions: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired
};

export default EditOptions;
