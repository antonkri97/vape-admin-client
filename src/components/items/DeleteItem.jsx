import React from 'react';
import PropTypes from 'prop-types';
import { Button, Row } from 'react-bootstrap';
 
const DeleteItem = ({ onRemoveOption, style }) => {
  return (
    <Row>
      <Button onClick={onRemoveOption} style={style} >
        Удалить
      </Button>
    </Row>
  )
};

DeleteItem.propTypes = {
  onRemoveOption: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired
};

export default DeleteItem;
