import React, { Component }       from 'react';
import PropTypes   from 'prop-types';
import Text        from './Text';
import TextArea    from './TextArea';
import Photo       from './Photo';
import Actions     from './Actions'

class Item extends Component {
  constructor(props) {
    super(props);

    const { name, vendor, description, largePhoto, mediumPhoto } = this.props;

    this.state = {
      name,
      vendor,
      description,
      largePhoto,
      mediumPhoto
    };

    this.handleState = this.handleState.bind(this);
  }

  handleState(e) {
    this.setState({
      [e.target.name]: e.target.value
    });

    console.log(this.state)
  }

  render() {
    const { id, name, vendor, description, onOpenOptions, onEditItem, largePhoto, mediumPhoto, onRemoveItem } = this.props

    return (
      <tr>
        <td>{id}</td>
        <Text formId='name' value={name} name='name' onChange={this.handleState}/>
        <Text formId='vendor' value={vendor} name='vendor' onChange={this.handleState}/>
        <TextArea formId='description' value={description} name='description' onChange={this.handleState}/>
        <Photo size='large' value={largePhoto} name='largePhoto' onChange={this.handleState}/>
        <Photo size='medium' value={mediumPhoto} name='mediumPhoto' onChange={this.handleState}/>
        <Actions onOpenOptions={onOpenOptions} onEditItem={() => onEditItem(this.state)} onRemoveItem={onRemoveItem}/>
      </tr>
    )
  }
}

Item.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  vendor: PropTypes.string.isRequired,
  description: PropTypes.string,
  largePhoto: PropTypes.string.isRequired,
  mediumPhoto: PropTypes.string.isRequired,
  onOpenOptions: PropTypes.func.isRequired,
  onEditItem: PropTypes.func.isRequired,
  onRemoveItem: PropTypes.func.isRequired
};

export default Item;
