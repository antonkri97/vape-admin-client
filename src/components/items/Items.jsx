import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';

const toInt = options => options.map(option => ({
  capacity: Number(option.capacity),
  nicotine: Number(option.nicotine),
  price: Number(option.price),
  quantity: Number(option.quantity)
}));

const Items = ({ items, onOpenOptions, onEditItem, onRemoveItem }) => (
  <tbody>
    {
      items.map((item, key) => (
        <Item
          key={key}
          id={item._id}
          name={item.name}
          vendor={item.vendor}
          description={item.description}
          largePhoto={item.largePhoto}
          mediumPhoto={item.mediumPhoto}
          onOpenOptions={() => onOpenOptions(item._id)}
          onEditItem={onEditItem(item._id, toInt(item.options))}
          onRemoveItem={() => onRemoveItem(item._id)}
        />
      ))
    }
  </tbody>
);

Items.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      vendor: PropTypes.string,
      description: PropTypes.string,
      options: PropTypes.arrayOf(PropTypes.shape({
        capacity: PropTypes.number,
        nicotine: PropTypes.number,
        price: PropTypes.number,
        quantity: PropTypes.number
      })),
      largePhoto: PropTypes.string,
      mediumPhoto: PropTypes.string
  })),
  onOpenOptions: PropTypes.func.isRequired,
  onEditItem: PropTypes.func.isRequired,
  onRemoveItem: PropTypes.func.isRequired
}

export default Items;
