import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '../Text';
import Actions   from './Actions';

class Option extends Component {
  constructor(props) {
    super(props);

    const { capacity, nicotine, price, quantity } = this.props;

    this.state = {
      capacity,
      nicotine,
      price,
      quantity
    }

    this.handleState = this.handleState.bind(this)
  }

  handleState(e) {
    this.setState({
      [e.target.name]: (Number(e.target.value))
    });

    console.log(this.state)
  }

  render() {
    const { index, capacity, nicotine, price, quantity, onRemoveOption, onEditOption } = this.props;

    return (
      <tr>
        <TextField
          formId={`${index}OptionCapacity`}
          value={capacity}
          name='capacity'
          onChange={this.handleState}
        />
        <TextField
          formId={`${index}OptionNicotine`}
          value={nicotine}
          name='nicotine'
          onChange={this.handleState}
        />
        <TextField
          formId={`${index}OptionPrice`}
          value={price}
          name='price'
          onChange={this.handleState}
        />
        <TextField
          formId={`${index}OptionQuantity`}
          value={quantity}
          name='quantity'
          onChange={this.handleState}
        />
        <Actions onRemoveOption={onRemoveOption} onEditOption={() => onEditOption(this.state)}/>
      </tr>
    )
  }
}

Option.propTypes = {
  index: PropTypes.number.isRequired,
  capacity: PropTypes.number.isRequired,
  nicotine: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  quantity: PropTypes.number.isRequired,
  onRemoveOption: PropTypes.func.isRequired,
  onEditOption: PropTypes.func.isRequired
};

export default Option;
