import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

const style = {
  'float': 'right'
};

const Close = ({ onClose }) => (
  <Button onClick={onClose} style={style}>
    Close
  </Button>
);

Close.propTypes = {
  onClose: PropTypes.func.isRequired
};

export default Close;
