import React from 'react';
import PropTypes from 'prop-types';
import Save from '../SaveEdit';
import Delete from '../DeleteItem';

const style = {
  'margin': '20px'
};

const Actions = ({ onSave, onRemoveOption, onEditOption }) => (
  <td>
    <Save onSaveEdit={onEditOption} style={style}/>
    <Delete onRemoveOption={onRemoveOption} style={style}/>
  </td>
);

Actions.propTypes = {
  onEditOption: PropTypes.func,
  onRemoveOption: PropTypes.func.isRequired
};

export default Actions;
