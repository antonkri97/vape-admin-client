import React from 'react';

const Header = () => (
  <thead>
    <tr>
      <th>Capacity</th>
      <th>Nicotine</th>
      <th>Price</th>
      <th>Quantity</th>
      <th>Actions</th>
    </tr>
  </thead>
);

export default Header;
