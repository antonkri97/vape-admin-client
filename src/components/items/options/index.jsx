import React     from 'react';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import Header    from './Header';
import Option    from './Option';
import Close     from './Close';
import Add       from './Add';

const OptionsTable = ({ id, options, onClose, onAddOption, onRemoveOption, onEditOption }) => {
  if (id === '-1'){
    return null;
  }
  return (
    <div>
      <Close onClose={onClose} />
      <Table striped bordered condensed hover>
        <Header />
        <tbody>
          {
            options.map((option, key) => (
              <Option
                key={key}
                index={key}
                capacity={option.capacity}
                nicotine={option.nicotine}
                price={option.price}
                quantity={option.quantity}
                onRemoveOption={() => onRemoveOption(id, key)}
                onEditOption={onEditOption(id, key)}
              />
            ))
          }
        </tbody>
      </Table>
      <Add onAddOption={() => onAddOption(id)}/>
    </div>
  )
}

OptionsTable.propTypes = {
  id: PropTypes.string,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      capacity: PropTypes.number,
      nicotine: PropTypes.number,
      price: PropTypes.number,
      quantity: PropTypes.number
    })
  ),
  onClose: PropTypes.func.isRequired,
  onAddOption: PropTypes.func.isRequired,
  onRemoveOption: PropTypes.func.isRequired,
  onEditOption: PropTypes.func.isRequired
};

export default OptionsTable;
