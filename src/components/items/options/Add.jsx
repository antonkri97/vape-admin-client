import React      from 'react';
import PropTypes  from 'prop-types';
import { Button } from 'react-bootstrap';

const style = {
  'float': 'right'
};

const Add = ({ onAddOption }) => (
  <Button onClick={onAddOption} style={style}>
    Add
  </Button>
);

Add.propTypes = {
  onAddOption: PropTypes.func.isRequired
};

export default Add;
