import React from 'react';

const Header = () => (
  <thead>
    <tr>
      <th>id</th>
      <th>Name</th>
      <th>Vendor</th>
      <th>Description</th>
      <th>Large photo</th>
      <th>Medium photo</th>
      <th>Actions</th>
    </tr>
  </thead>
)

export default Header;
