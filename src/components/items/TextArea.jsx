import React                      from 'react';
import PropTypes                  from 'prop-types';
import { FormGroup, FormControl } from 'react-bootstrap';

const TextArea = ({ formId, value, ...props }) => (
  <td>
    <FormGroup controlId={formId}>
      <FormControl componentClass="textarea" defaultValue={value} {...props} />
    </FormGroup>
  </td>
);

TextArea.propTypes = {
  formId: PropTypes.string.isRequired,
  value: PropTypes.string
};

export default TextArea;
