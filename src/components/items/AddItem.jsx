import React from 'react';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

const style = {
  'float': 'right',
  'marginBottom': '2%'
};

const AddItem = ({ onAddItem }) => (
  <Button onClick={onAddItem} style={style}>
    Add item
  </Button>
);

AddItem.propTypes = {
  onAddItem: PropTypes.func.isRequired
};

export default AddItem;
