import React                from 'react';
import PropTypes            from 'prop-types';
import { Grid, PageHeader } from 'react-bootstrap';

const App = ({ children }) => (
  <div>
    <PageHeader className='text-center'>Vape Admin</PageHeader>
    <Grid>
      {children}
    </Grid>
  </div>
);

App.propTypes = {
  children: PropTypes.node.isRequired
};

export default App;
