import React                            from 'react';
import routes                           from './routes';
import ReactDOM                         from 'react-dom'
import reducers                         from './reducers';
import logger                           from 'redux-logger';
import thunk                            from 'redux-thunk';
import { Provider }                     from 'react-redux';
import { Router, browserHistory }       from 'react-router';
import { createStore, applyMiddleware } from 'redux';

import './style/bootstrap.css';

const store = createStore(reducers, applyMiddleware(thunk, logger));

const component = (
  <Provider store={store}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </Provider>
)

ReactDOM.render(component, document.getElementById('root'));
